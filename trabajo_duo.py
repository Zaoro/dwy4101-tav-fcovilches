from statistics import mode
# # 1

a=1
e=2
i=3
o=4
u=5
c1=a+e+e+i+o+u+e+u+a
c2=o+i+e+u+a
c3=a+i+u+e+e+o
ct=c1+c2+c3
print ("el resultado de las 3 cadenas es: ",ct)

# # 10

a=0
b=0
print("Ingrese un numero de 3 digitos, negativo o positivo")
respuesta=float(input())

if respuesta < 1000:
    if respuesta > -1000:
        if respuesta >= 100:
            print("funciona")
            b=respuesta
            print(b)
        else:
            print("El numero debe ser mayor a 2 digitos")
        if respuesta <= -100:
            print("funciona -5")
            a=respuesta
            print(a)
        else:
            print("El numero debe ser mayor a 2 digitos")
    else:
        print("El numero ingresado debe tener una longitudn de 3 digitos")        
else:
    print("El numero ingresado debe tener una longitudn de 3 digitos")

# # 9

mensaje9 = input("ingrese una palabra ")
startLoc1 = 0
endLoc1 = 1
startLoc2 = 4
endLoc2 = 5
contador=0
while contador<2:
    mensaje9b1 = mensaje9[startLoc1: endLoc1]
    mensaje9b2 = mensaje9[startLoc2: endLoc2]
    print(mensaje9b1," ",mensaje9b2)
    if mensaje9b1==mensaje9b2:
        print("exito")
        startLoc1=startLoc1+1
        endLoc1=endLoc1+1
        startLoc2=startLoc2-1
        endLoc2=endLoc2-1
        contador=contador+1
        if contador==2:
            print("si es un palindromo")
    else:
        print("fallo")
        print("no es un palindromo")
        contador=contador+10

# # 14

lista = [1,6,4,4,5,2,7,4,5,8]
promedio=0
ponderado=0
ponderado1=0
ponderado2=0
mayor=0
contador=1
for cuenta in lista:
    promedio=promedio+cuenta
for cuenta in lista[0:5]:
    ponderado1=ponderado1+(cuenta)
for cuenta in lista[5:10]:
    ponderado2=ponderado2+(cuenta)
ponderado1=(ponderado1/5)*0.3
ponderado2=(ponderado2/5)*0.7
ponderado=ponderado1+ponderado2
print("El promedio fue de:",promedio/10)
print("La mayor nota fue de:",max(lista))
print("La menor nota fue de:",min(lista))
print("La nota mas repetida fue de:",mode(lista))
print("El promedio ponderado es:",ponderado)

# # 15

asistencia = [0,0,1,1,1]
notaTaller = [4.3,3.2,5.0,5.5,6.0]
notaControl = [4.5,6.8,3.2,3.5,7.0]

promCont = sum(notaControl)/5
promTal = sum(notaTaller)/5
print("el promedio de controles es: ",promCont)
print("el promedio de Taller es: ",promTal)

promF = (promTal*0.5)+(promCont*0.4)+ (sum(asistencia)*0.1)
print(" su promedio final es: ",promF)