from django.shortcuts import render
from django.shortcuts import redirect
from .models import Gato
from .forms import GatoForm

def inicio(request):
    return render(request,'misgatis/index.html')

def agregar(request):
    if request.method == 'POST':
        gato = Gato.objects.create()
        gato.nombre_gato=request.POST['nombre_gato']
        gato.meses=request.POST['meses']
        gato.ciudad=request.POST['ciudad']
        gato.region=request.POST['region']
        gato.telefono=request.POST['telefono']
        gato.save()
        gatos = Gato.objects.all()
        return render(request,'misgatis/listar.html',{'gatos': gatos})
    else:
        form = GatoForm()
        return render(request,'misgatis/formulario.html')

def listar(request):
	gatos = Gato.objects.all()
	return render(request, 'misgatis/listar.html',{'gatos': gatos})