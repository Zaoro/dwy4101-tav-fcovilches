from django.urls import path
from . import views

urlpatterns = [
    path('', views.inicio, name='inicio'),
    path('agregar',views.agregar, name='agregar'),
    path('listar',views.listar,name="listar"),
]