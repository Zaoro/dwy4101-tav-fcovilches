from django.db import models

class Gato(models.Model):
    nombre_gato = models.TextField(null=False,blank=False)
    meses = models.TextField(null=False,blank=False)
    ciudad = models.TextField(null=False,blank=False)
    region = models.TextField(null=False,blank=False)
    telefono = models.TextField(null=False,blank=False)