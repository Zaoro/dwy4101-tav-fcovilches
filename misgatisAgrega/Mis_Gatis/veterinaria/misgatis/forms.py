from django import forms
from misgatis.models import Gato

class GatoForm(forms.ModelForm):

    class Meta:
        model = Gato
        fields = ('nombre_gato','meses','ciudad','region','telefono',)